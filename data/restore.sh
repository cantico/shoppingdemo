#!/bin/bash

# 1 - empty folder
# 2 - copy all files except data/ to folder
# 3 - import database
# 4 - use composer to install dependencies
# 5 - move the core folder





# Work on files

if [ ! -f www/config.php ]
then
    echo "This program need to be launched in the shoppingdemo base folder"
    exit;
fi


. data/database.conf

SHOP_PATH=${1}

if [ -z "$SHOP_PATH" ]
then 
    echo "path to web folder is required, the content will be erased"
    exit;
fi

rm -Rf "${SHOP_PATH}"
mkdir "${SHOP_PATH}"


cp -rf www/* "$SHOP_PATH/"
cp data/database.conf "$SHOP_PATH/"


# Work on database


newpass=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c10);
echo $newpass>data/adminpassword
md5pass=$(php -r "echo md5(strtolower('${newpass}'));")

mysql -u $babDBLogin -p$babDBPasswd $babDBName < data/database.sql
mysql -u $babDBLogin -p$babDBPasswd -e "UPDATE bab_users SET password='${md5pass}' WHERE nickname='admin@admin.bab'" $babDBName

cd "$SHOP_PATH/"


# /usr/local/bin/ is not in the PATH in /etc/crontab

/usr/local/bin/composer --no-interaction install --no-dev

cp -rf vendor/ovidentia/ovidentia/ovidentia ./ovidentia

if id -u "apache" >/dev/null 2>&1; then
        chown -R apache:apache *
else
        chown -R www-data:www-data *
fi

date "+%T" > "$SHOP_PATH/vendor/ovidentia/theme_shopping/theme/ovml/last_database_update"
