## Installation

~~~bash
git clone https://bitbucket.org/cantico/shoppingdemo.git
cd shoppingdemo/
cp data/database.conf.default data/database.conf
~~~

Définir les informations pour accéder à la base de donnés mysql dans le fichier database.conf

~~~
nano data/database.conf
~~~

Intéger cette commande de réinitialisation dans une tâche cron:

~~~
git pull && ./data/restore.sh /home/www/demoboutique
~~~

Cette commande est a lancer depuis le dossier de shoppingdemo


## Accès

A chaque initialisation de l'environement, un nouveau mot de passe pour le compte admin@admin.bab sera déposé dans le fichier data/adminpassword

Aucun autre administrateur ne doit être créé dans la base et exporté sur data/database.sql pour garantir la sécurité du serveur


Pour accéder en tant que gestionnaire à la boutique, il faut utiliser:

* email: manager@example.com
* mot de passe: secret